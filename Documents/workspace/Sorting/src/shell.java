
public class shell {
	public static void sort(int[] a)
	{
		int N=a.length;
		int h=1;
		while(h<N/3)
		{
			h=3*h+1;
		}
		while(h>=1)
		{
			for(int i=h;i<N;i++)
			{
				for(int j=i;j>=h;j=j-h)
				{
					if(a[j-h]>a[j])
					{
						int temp=a[j-h];
						a[j-h]=a[j];
						a[j]=temp;
					}
					else
					{
						break;
					}
				}
			}
			h=h/3;
		}
	}
	public static void main(String[] args)
	{
		int[] a={0,4,8,10,67,3,78,90,24};
		sort(a);
		for(int i=0;i<a.length;i++)
		{
			System.out.print(a[i]);
		}
	}
}
